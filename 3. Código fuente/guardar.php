<?php
require 'conexion.php';
require_once('validaciones.php');

$titulo = empty($_POST['titulo']) ? "" : htmlentities($_POST['titulo']);
$autor = empty($_POST['autor']) ? "" : htmlentities($_POST['autor']);
$division = empty($_POST['division']) ? "" : htmlentities($_POST['division']);
$contacto = empty($_POST['contacto']) ? "" : htmlentities($_POST['contacto']);
$correo = empty($_POST['correo']) ? "" : htmlentities($_POST['correo']);
$telof = empty($_POST['telof']) ? "" : htmlentities($_POST['telof']);
$telpar = empty($_POST['telpar']) ? "" : htmlentities($_POST['telpar']);
$instrucciones = empty($_POST['instrucciones']) ? "" : htmlentities($_POST['instrucciones']);
$aviso = empty($_POST['aviso']) ? "" : htmlentities($_POST['aviso']);


$validacionTitulo = validarTexto($titulo);
$validacionAutor = validarTexto($autor);
$validacionContacto = validarTexto($contacto);
$validacionCorreo = validarCorreo($correo);
$validacionTelof = validarTel($telof);
$validacionTelpar = validarTel($telpar);

$validacionTodos = $validacionTitulo && $validacionAutor && $validacionContacto && $validacionCorreo && $validacionTelof && $validacionTelpar;



if ($validacionTodos) {
	
	$sql = "INSERT INTO libro VALUES ('$titulo','$autor','$division','$contacto','$correo','$telof','$telpar','$instrucciones','$aviso')";

    $query = mysqli_query($conectar, $sql);

    if($query){
        echo "<script> alert('Los datos fueron enviados correctamente'); 
        location.href = 'solicitudes.php';
        </script>";
        
    }else{
        echo "<script> alert('Hubo un error en el envío de los datos'); 
        location.href = 'solicitudes.php';
        </script>";
    }		

}else{
    echo "<script> alert('No fue enviada su solicitud, revise los datos ingresados'); 
        location.href = 'solicitudes.php';
        </script>";
}

?>
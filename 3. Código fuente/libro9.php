<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Libros Facultad de Ingeniería UNAM</title>

    <link href="fonts/all.css" rel="stylesheet" />
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    
    <link href="css/comun.css" rel="stylesheet" />
    <link href="css/principal.css" rel="stylesheet" />

    

</head>

<body>

    <header id="demo">

        <div class="encabezado">

            <div class="escudos">

                <img src="img/escudo_unam_negro.png" alt="UNAM"/>
                <img src="img/escudo_fi_rojo.png" alt="FI"/>
            
            </div>

            <div class="buscador">

                <label for="buscar">Burcar</label>
                <input id="buscar" type="text" name="buscar" placeholder="Buscar por nombre, autor o título" maxlength="128" value="">
            
                <button type="submit" title="Buscar libro" alt="Burcar libro" class="fa-solid fa-magnifying-glass"><span>Buscar</span></button>
            </div>

            <div class="logo">
                <img src="img/logo_udae.png" alt="UDAE"/>
            </div>
            

        </div> <!-- /encabezado -->

        <nav class="menu-principal">

            <ul>
                <li><a href="index.php">Inicio</a></li>
                <li><a href="catalogo.php">Catálogo</a></li>
                <li><a href="index.php#novedades" target="_blank">Novedades</a></li>
                <li><a href="index.php#mas_descargados" target="_parent">Más descargados</a></li>
                <li><a href="index.php#recomendaciones" target="_parent">Recomendaciones</a></li>
            </ul>

        </nav>   
        
        <div class="titulo-pagina">
            <h1>Recomendaciones</h1>
            
        </div>

    </header>

   

    <main class="container">        

        <section class="ver-mas row mt-5">

            <h2>Fundamentos de seguridad informática</h2>

            <article class="d-flex flex-row justify-content-center alig-items-center col-md-8 col-lg-8">

                <img src="img/libro_9.jpg" alt="Fundamentos de seguridad informática"/>

            </article>

            <article class="border-top border-4 my-auto col-md-4 col-lg-4">
                <div>
                    <li><b>Título: </b><em>Fundamentos de seguridad informática</em></li>
                    <li><b>Autor: </b>Ma. Jaquelina López Barrientos y Cintia Quezada Reyes </li>
                    <li><b>Fecha de publicación:</b> noviembre 2019</li>
                    <li><b>Área de conocimiento:</b>División de Ingeniería Eléctrica</li>
                </div>
            </article>

        
        </section> <!-- /ficha -->

        <section class="ver-mas row mt-5">

            
            <article class="">

                <p>México, UNAM, Facultad de Ingeniería, 2ª. Edición 2019, 354 p., tiraje 300 ejemplares.</p>

                <p>En esta segunda edición de la obra Fundamentos de seguridad informática se ha buscado en todo momento abordar los temas que se consideran indispensables en la formación de los futuros ingenieros que buscan especializarse en el ámbito de la seguridad informática. Este material incorpora conocimientos no contemplados en la primera edición, complementa la teoría con actualizaciones y brida al lector la oportunidad de reconocer su propio avance a través de las preguntas de evaluación.</p>

                <p>Este libro plantea el estudio de los fundamentos de la seguridad informática que deben conocerse si se está interesado en la protección de los bienes informáticos, en especial, los ingenieros en computación cuyos conocimientos deben permitir concientizar a los usuarios en que la seguridad informática es una inversión que se basa en un ciclo de administración y mejora continua, y en poner en práctica sus conocimientos y habilidades para crear ambientes informáticos seguros, evita el desaprovechamiento de los recursos, los gastos excesivos y problemas futuros debido a brechas de seguridad existentes en el ámbito laboral y personal. </p>

                <p><b>CONTENIDO.</b> Prólogo; Agradecimientos; Contexto de la seguridad; Normatividad; Amenazas y vulnerabilidades; Servicios de seguridad; Análisis del riesgo; Herramientas de seguridad; Ética informática; Legislación informática; Glosario; Bibliografía.</p>
            </article>

           
        </section> <!-- /resumen -->


        <!--<button onclick="topFunction()" id="myBtn" title="Go to top">Top</button>-->
        <a href="#demo" id="myBtn" title="Go to top">Subir</a>

    </main>

    <footer>

        <div class="footer-gris">

            <div>

                <img src="img/logofooter.png" alt="Facultad de Ingeniería"/>

                <div>

                    <input class="boton-contacto" type="button" value="Contacto" />
    
                </div>
            
            </div>

            <div>

                <p>
                    Universidad Nacional Autónoma de México<br/>
                    Facultad de Ingeniería, Av. Universidad 3000, Ciudad Universitaria,<br/> 
                    Coyoacán, Cd. Mx., CP 04510<br/>
                    Teléfono: 56 22 08 66<br/>
                    Fax: 56 16 28 90<br/>
                    <a href="mailto:fainge@unam.mx">fainge@unam.mx</a>

                    <div class="mt-3 text-center">
                        <img src="img/ico_face.jpg" data-aos="zoom-in" class="px-2 ">
                        <img src="img/ico_twit.jpg" data-aos="zoom-in" class="px-2">
                        <img src="img/ico_g_plus.jpg" data-aos="zoom-in" class="px-2">
                    </div>
                
                </p>
            
            </div>

            <div class="sitios-interes">

                <p>Sitios de interés</p>
                <ul>
                    <li><a href="solicitudes.php">Solicitudes de edición</a></li>
                    <li><a href="http://www.ptolomeo.unam.mx:8080/xmlui/" target="blank">Repositorio Digital FI</a></li>
                </ul>
            
            </div>

        </div>

        <div class="footer-rojo">

            <p>
                Esta página puede ser reproducida con fines no lucrativos, siempre y cuando no se mutile, se cite la fuente completa y su dirección electrónica. 
            </p>
        
        </div>
        

    </footer>


    <script>
        //Get the button
        var mybutton = document.getElementById("myBtn");
        
        // When the user scrolls down 20px from the top of the document, show the button
        window.onscroll = function() {scrollFunction()};
        
        function scrollFunction() {
          if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            mybutton.style.display = "block";
          } else {
            mybutton.style.display = "none";
          }
        }
        
        // When the user clicks on the button, scroll to the top of the document
        function topFunction() {
          document.body.scrollTop = 0;
          document.documentElement.scrollTop = 0;
        }
    </script>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

    <script src ="js/smooth.js"></script>
    <script src ="js/scroll_line.js"></script>


    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

    <script>
      AOS.init();
    </script>


</body>
</html>


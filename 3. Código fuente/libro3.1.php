<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Libros Facultad de Ingeniería UNAM</title>

    <link href="fonts/all.css" rel="stylesheet" />
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    
    <link href="css/comun.css" rel="stylesheet" />
    <link href="css/principal.css" rel="stylesheet" />

    

</head>

<body>

    <header id="demo">

        <div class="encabezado">

            <div class="escudos">

                <img src="img/escudo_unam_negro.png" alt="UNAM"/>
                <img src="img/escudo_fi_rojo.png" alt="FI"/>
            
            </div>

            <div class="buscador">

                <label for="buscar">Burcar</label>
                <input id="buscar" type="text" name="buscar" placeholder="Buscar por nombre, autor o título" maxlength="128" value="">
            
                <button type="submit" title="Buscar libro" alt="Burcar libro" class="fa-solid fa-magnifying-glass"><span>Buscar</span></button>
            </div>

            <div class="logo">
                <img src="img/logo_udae.png" alt="UDAE"/>
            </div>
            

        </div> <!-- /encabezado -->

        <nav class="menu-principal">

            <ul>
                <li><a href="index.php">Inicio</a></li>
                <li><a href="catalogo.php">Catálogo</a></li>
                <li><a href="index.php#novedades" target="_parent">Novedades</a></li>
                <li><a href="index.php#mas_descargados" target="_parent">Más descargados</a></li>
                <li><a href="index.php#recomendaciones" target="_parent">Recomendaciones</a></li>
            </ul>

        </nav>   
        
        <div class="titulo-pagina">
            <h1>Novedades</h1>
            
        </div>

    </header>

   

    <main class="container">        

        <section class="ver-mas row mt-5">

            <h2>Apuntes de geoquímica</h2>

            <article class="d-flex flex-row justify-content-center alig-items-center col-md-8 col-lg-8">

                <img src="img/libro_3.1.jpg" alt="Apuntes de geoquímica"/>

            </article>

            <article class="border-top border-4 my-auto col-md-4 col-lg-4">
                <div>
                    <li><b>Título: </b><em>Apuntes de geoquímica</em></li>
                    <li><b>Autor: </b>Laura Mori y Eduardo Becerra Torres</li>
                    <li><b>Fecha de publicación:</b> septiembre 2021</li>
                    <li><b>Área de conocimiento:</b> División de Ingeniería en Ciencias de la Tierra</li>
                </div>
            </article>

        
        </section> <!-- /ficha -->

        <section class="ver-mas row mt-5">

            
            <article class="">

                <p>México, Universidad Nacional Autónoma de México – Facultad de Ingeniería, 2022, 173 pp. ISBN 978-607-30-6449-1. </p>

                <p>Obra electrónica disponible en:</p>
                    
                <p><a href="http://www.ptolomeo.unam.mx:8080/xmlui/handle/RepoFi/17995" target="_blank">http://www.ptolomeo.unam.mx:8080/xmlui/handle/RepoFi/17995</a></p>

                <p>Esta obra forma parte de una colección más amplia de recursos didácticos que incluye videos, cuestionarios de autoevaluación y actividades acompañadas de rúbricas de evaluación, los cuales se han elaborado para las y los estudiantes de Ingeniería Geológica de la Facultad de Ingeniería de la UNAM con el objetivo de fomentar un aprendizaje significativo, consciente y responsable de la geoquímica.</p>

                <p>La obra, que cubre la información esencial contemplada en el programa de la asignatura de Geoquímica, está organizada en tres grandes bloques temáticos. En el bloque 1. Geoquímica de la Tierra silicatada, se introduce la geoquímica de elementos mayores, traza e isótopos radiogénicos en sistemas magmáticos como una herramienta para analizar los procesos involucrados en la génesis y evolución de los reservorios rocosos de la Tierra (manto, corteza oceánica y masas continentales). En el bloque 2. Geoquímica de las aguas naturales, se describen los procesos que afectan la capacidad reactiva de las especies disueltas en las soluciones acuosas naturales y se examina la sistemática de los isótopos estables de oxígeno en el ciclo hidrológico como una herramienta para realizar reconstrucciones paleoclimáticas. Finalmente, en el bloque 3. Interacción agua-roca, se analizan los procesos de meteorización química que afectan las rocas expuestas en la superficie terrestre y se exploran los procesos de adsorción e intercambio iónico a través de los cuales los minerales arcillosos derivados de la meteorización interactúan con las aguas naturales.</p>
                    
                <p><b>CONTENIDO.</b> Prefacio. 1. Geoquímica de la Tierra silicatada: Del Big Bang a la Tierra; Elementos mayores y traza en sistemas magmáticos; Isótopos radiogénicos en sistemas magmáticos; Geoquímica del manto; Geoquímica de la corteza oceánica; Geoquímica de la corteza continental. 2. Geoquímica de las aguas naturales: Las aguas naturales; Los isótopos estables en el ciclo hidrológico. 3. Interacción agua-roca: Procesos de meteorización química; Los minerales arcillosos y sus interacciones con las aguas naturales. Referencias bibliográficas.</p>
            </article>

           
        </section> <!-- /resumen -->


        <!--<button onclick="topFunction()" id="myBtn" title="Go to top">Top</button>-->
        <a href="#demo" id="myBtn" title="Go to top">Subir</a>

    </main>

    <footer>

        <div class="footer-gris">

            <div>

                <img src="img/logofooter.png" alt="Facultad de Ingeniería"/>

                <div>

                    <input class="boton-contacto" type="button" value="Contacto" />
    
                </div>
            
            </div>

            <div>

                <p>
                    Universidad Nacional Autónoma de México<br/>
                    Facultad de Ingeniería, Av. Universidad 3000, Ciudad Universitaria,<br/> 
                    Coyoacán, Cd. Mx., CP 04510<br/>
                    Teléfono: 56 22 08 66<br/>
                    Fax: 56 16 28 90<br/>
                    <a href="mailto:fainge@unam.mx">fainge@unam.mx</a>

                    <div class="mt-3 text-center">
                        <img src="img/ico_face.jpg" data-aos="zoom-in" class="px-2 ">
                        <img src="img/ico_twit.jpg" data-aos="zoom-in" class="px-2">
                        <img src="img/ico_g_plus.jpg" data-aos="zoom-in" class="px-2">
                    </div>
                
                </p>
            
            </div>

            <div class="sitios-interes">

                <p>Sitios de interés</p>
                <ul>
                    <li><a href="solicitudes.php">Solicitudes de edición</a></li>
                    <li><a href="http://www.ptolomeo.unam.mx:8080/xmlui/" target="blank">Repositorio Digital FI</a></li>
                </ul>
            
            </div>

        </div>

        <div class="footer-rojo">

            <p>
                Esta página puede ser reproducida con fines no lucrativos, siempre y cuando no se mutile, se cite la fuente completa y su dirección electrónica. 
            </p>
        
        </div>
        

    </footer>


    <script>
        //Get the button
        var mybutton = document.getElementById("myBtn");
        
        // When the user scrolls down 20px from the top of the document, show the button
        window.onscroll = function() {scrollFunction()};
        
        function scrollFunction() {
          if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            mybutton.style.display = "block";
          } else {
            mybutton.style.display = "none";
          }
        }
        
        // When the user clicks on the button, scroll to the top of the document
        function topFunction() {
          document.body.scrollTop = 0;
          document.documentElement.scrollTop = 0;
        }
    </script>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

    <script src ="js/smooth.js"></script>
    <script src ="js/scroll_line.js"></script>


    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

    <script>
      AOS.init();
    </script>


</body>
</html>


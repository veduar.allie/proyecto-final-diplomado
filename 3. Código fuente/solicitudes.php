<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Libros Facultad de Ingeniería UNAM</title>

    <link href="fonts/all.css" rel="stylesheet" />
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    
    <link href="css/comun.css" rel="stylesheet" />
    <link href="css/solicitud.css" rel="stylesheet" />
    <link href="css/principal.css" rel="stylesheet" />

    

</head>

<body>

    <header id="demo">

        <div class="encabezado">

            <div class="escudos">

                <img src="img/escudo_unam_negro.png" alt="UNAM"/>
                <img src="img/escudo_fi_rojo.png" alt="FI"/>
            
            </div>

            <div class="buscador">

                <label for="buscar">Burcar</label>
                <input id="buscar" type="text" name="buscar" placeholder="Buscar por nombre, autor o título" maxlength="128" value="">
            
                <button type="submit" title="Buscar libro" alt="Burcar libro" class="fa-solid fa-magnifying-glass"><span>Buscar</span></button>
            </div>

            <div class="logo">
                <img src="img/logo_udae.png" alt="UDAE"/>
            </div>
            

        </div> <!-- /encabezado -->

        <nav class="menu-principal">

            <ul>
                <li><a href="index.php">Inicio</a></li>
                <li><a href="index.php#novedades" target="_blank">Novedades</a></li>
                <li><a href="index.php#mas_descargados" target="_parent">Más descargados</a></li>
                <li><a href="index.php#recomendaciones" target="_parent">Recomendaciones</a></li>
            </ul>

        </nav>   
        
        <div class="titulo-pagina">
            <h1>Solicitudes de edición de libros</h1>
            
        </div>

    </header>

   

    <main class="container">        

        <section class="ver-mas row mt-5">

            <h2>Procedimiento de Solicitud</h2>

            <br/>
            <p>Para solicitar la edición de un material didáctico escrito es necesario presentarlo acompañado de un oficio dirigido al Secretario General de la Facultad con el visto bueno del Jefe de la División, en el que se indique el título completo de la obra, los autores y la(s) asignatura(s) que apoya.</p>
            
            <p>Es requisito indispensable adjuntar el formato de <a href="docs/declaracion_de_originalidad_y_honestidad_academica.pdf" target="_blank">"Declaración de originalidad y ética académica"</a> debidamente firmado.</p>
            
            <p>Todo material que pretenda ser publicado deberá someterse al Comité Editorial y, posteriormente, tendrá que incorporar las observaciones que sugiera el dictamen técnico emitido.
            </p>
            
            <p>El trámite de solicitud de asignación de ISBN ante la Dirección General de Asuntos Jurídicos, se realizará para las obras que hayan sido dictaminadas favorablemente y hayan incorporado las correcciones surgidas de la revisión técnica.</p>

            <p><a href="#cuestionario">Llenar el siguiente cuestionario y enviarlo</a></p>

            <form id="cuestionario" action="guardar.php" method="post" enctype="multipart/form-data">

                <input type="hidden" name="id" value="1001" />
    
                <br/>
                <fieldset>
    
                    <legend>Información del libro</legend>
    
                    <p>
                        <label for="titulo">Título del libro:</label> 
                        <input id="titulo" type="text" name="titulo"/>
                    </p>
        
                    <p>
                        <label for="autor">Nombre del primer autor:</label> 
                        <input id="autor" type="text" name="autor"/>
                    </p>

                    <p>
                        <label for="division">Seleccionar la división de adscripción del primer autor</label>
                        <select id="division" name="division">
                            <option value="dcb" selected>División de Ciencias Básicas</option>
                            <option value="dicyg">División del Ingenierías Civil y Geomática</option>
                            <option value="die">Dvisión de Ingeniería Eléctrica</option>
                            <option value="dimei">División de Ingeniería Mecánica e Industrial</option>
                            <option value="dict">División de Ingeniería en Ciencias de la Tierra</option>
                            <option value="dcsyh">División de Ciencias Sociales y Humanidades</option>
                        </select>
                    </p>
    
                </fieldset>
                
                <br/>
                <fieldset>
                    <legend>Datos de contacto</legend>
    
                    <p>
                        <label for="contacto">Nombre del académico de contacto:</label> 
                        <input id="contacto" type="text" name="contacto"/>
                    </p>
        
                    <p>
                        <label for="correo">Correo electrónico:</label> 
                        <input id="correo" type="text" name="correo"/>
                    </p>

                    <p>
                        <label for="telof">Teléfono de oficina:</label> 
                        <input id="telof" type="text" name="telof"/>
                    </p>

                    <p>
                        <label for="telpar">Teléfono particular:</label> 
                        <input id="telpar" type="text" name="telpar"/>
                    </p>

                    
                    <br/>
                    <p>
                        <input type="checkbox" id="instrucciones" name="instrucciones" value="acepto"/>
                        <label for="instrucciones"><a href="docs/instrucciones.pdf" target="_blank">Acepto que he leído las instrucciones</a></label>
                        <br/>
                        <input type="checkbox" id="aviso" name="aviso" value="acepto2"/>
                        <label for="aviso"><a href="https://www.ingenieria.unam.mx/paginas/aviso_privacidad.php" target="_blank">Acepto que he leído el aviso de privacidad de la Facultad de Ingeniería, UNAM</a></label>
                    </p>

    
                </fieldset>
                
                <br/>
                <div class="contenedor-botones">
    
                    <input class="boton-registro" type="submit" value="Enviar" />
                    <input class="boton-registro" type="reset" value="Limpiar" />
    
                </div>
      
            </form>
    

        
        </section> <!-- /ficha -->


        <!--<button onclick="topFunction()" id="myBtn" title="Go to top">Top</button>-->
        <a href="#demo" id="myBtn" title="Go to top">Subir</a>

    </main>

    <footer>

        <div class="footer-gris">

            <div>

                <img src="img/logofooter.png" alt="Facultad de Ingeniería"/>

                <div>

                    <input class="boton-contacto" type="button" value="Contacto" />
    
                </div>
            
            </div>

            <div>

                <p>
                    Universidad Nacional Autónoma de México<br/>
                    Facultad de Ingeniería, Av. Universidad 3000, Ciudad Universitaria,<br/> 
                    Coyoacán, Cd. Mx., CP 04510<br/>
                    Teléfono: 56 22 08 66<br/>
                    Fax: 56 16 28 90<br/>
                    <a href="mailto:fainge@unam.mx">fainge@unam.mx</a>

                    <div class="mt-3 text-center">
                        <img src="img/ico_face.jpg" data-aos="zoom-in" class="px-2 ">
                        <img src="img/ico_twit.jpg" data-aos="zoom-in" class="px-2">
                        <img src="img/ico_g_plus.jpg" data-aos="zoom-in" class="px-2">
                    </div>
                
                </p>
            
            </div>

            <div class="sitios-interes">

                <p>Sitios de interés</p>
                <ul>
                    <li><a href="solicitudes.php">Solicitudes de edición</a></li>
                    <li><a href="http://www.ptolomeo.unam.mx:8080/xmlui/" target="blank">Repositorio Digital FI</a></li>
                </ul>
            
            </div>

        </div>

        <div class="footer-rojo">

            <p>
                Esta página puede ser reproducida con fines no lucrativos, siempre y cuando no se mutile, se cite la fuente completa y su dirección electrónica. 
            </p>
        
        </div>
        

    </footer>


    <script>
        //Get the button
        var mybutton = document.getElementById("myBtn");
        
        // When the user scrolls down 20px from the top of the document, show the button
        window.onscroll = function() {scrollFunction()};
        
        function scrollFunction() {
          if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            mybutton.style.display = "block";
          } else {
            mybutton.style.display = "none";
          }
        }
        
        // When the user clicks on the button, scroll to the top of the document
        function topFunction() {
          document.body.scrollTop = 0;
          document.documentElement.scrollTop = 0;
        }
    </script>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

    <script src ="js/smooth.js"></script>
    <script src ="js/scroll_line.js"></script>


    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

    <script>
      AOS.init();
    </script>


</body>
</html>


<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Libros Facultad de Ingeniería UNAM</title>
    <meta name="description" content="Descarga libros gratuitos editados por profesores de la Facultad de Ingeniería de la UNAM.">

    <link href="fonts/all.css" rel="stylesheet" />
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    
    <link href="css/comun.css" rel="stylesheet" />
    <link href="css/principal.css" rel="stylesheet" />

    <!--Favicon-->
    <link rel="apple-touch-icon" sizes="57x57" href="iconos.ico/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="iconos.ico/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="iconos.ico/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="iconos.ico/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="iconos.ico/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="iconos.ico/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="iconos.ico/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="iconos.ico/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="iconos.ico/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="iconos.ico/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="iconos.ico/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="iconos.ico/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="iconos.ico/favicon-16x16.png">
    <link rel="manifest" href="iconos.ico/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

</head>

<body>

    <header id="demo">

        <div class="encabezado">

            <div class="escudos">

                <img src="img/escudo_unam_negro.png" alt="UNAM"/>
                <img src="img/escudo_fi_rojo.png" alt="FI"/>
            
            </div>

            <div class="buscador">

                <label for="buscar">Burcar</label>
                <input id="buscar" type="text" name="buscar" placeholder="Buscar por nombre, autor o título" maxlength="128" value="">
            
                <button type="submit" title="Buscar libro" alt="Burcar libro" class="fa-solid fa-magnifying-glass"><span>Buscar</span></button>
            </div>

            <div class="logo">
                <img src="img/logo_udae.png" alt="UDAE"/>
            </div>
            

        </div> <!-- /encabezado -->

        <nav class="menu-principal">

            <ul>
                <li><a href="catalogo.php">Catálogo</a></li>
                <li><a href="#novedades">Novedades</a></li>
                <li><a href="#mas_descargados">Más descargados</a></li>
                <li><a href="#recomendaciones">Recomendaciones</a></li>
            </ul>

        </nav>   
        
        <div class="titulo-pagina">
            <h1>Libros gratuitos de la Facultad de Ingeniería</h1>
        </div>

    </header>

   

    <main class="container">

        <!-- Carousel -->
        <div  class="carousel slide" data-bs-ride="carousel">

            <!-- Indicators/dots -->
            <div class="carousel-indicators">
            <button type="button" data-bs-target="#demo" data-bs-slide-to="0" class="active"></button>
            <button type="button" data-bs-target="#demo" data-bs-slide-to="1"></button>
            <button type="button" data-bs-target="#demo" data-bs-slide-to="2"></button>
            </div>
            
            <!-- The slideshow/carousel -->
            <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="img/banner1.jpg" alt="Descarga libros" class="d-block" style="width:100%">
            </div>
            <div class="carousel-item">
                <img src="img/banner2.jpg" alt="Presentación de libro" class="d-block" style="width:100%">
            </div>
            <div class="carousel-item">
                <img src="img/banner3.jpg" alt="Trivia" class="d-block" style="width:100%">
            </div>
            </div>
            
            <!-- Left and right controls/icons -->
            <button class="carousel-control-prev" type="button" data-bs-target="#demo" data-bs-slide="prev">
            <span class="carousel-control-prev-icon"></span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#demo" data-bs-slide="next">
            <span class="carousel-control-next-icon"></span>
            </button>
        </div>

        

        <section id="novedades" class="row mt-5">

            <h2>Novedades</h2>

            <article class="col-md-6 col-lg-3">
                <figure>

                    <a href="libro1.php" target="_parent"><img src="img/portada_1.jpg" alt="Programación dinámica"/></a>
                    <figcaption class="enlace"><a href="libro1.php" target="_parent">Programación dinámica</a></figcaption>

                </figure>
            </article>

            <article class="col-md-6 col-lg-3">
                <figure>

                    <a href="libro2.php" target="_parent"><img src="img/portada_2.jpg" alt="Evaluación de la calidad del agua mediante técnicas de percepción remota"/></a>
                    <figcaption class="enlace"><a href="libro2.php" target="_parent">Evaluación de la calidad del agua mediante técnicas de percepción remota</a></figcaption>
                
                </figure>
            </article>

            <article class="col-md-6 col-lg-3">
                <figure>

                    <a href="libro3.php" target="_parent"><img src="img/portada_3.jpg" alt="La construcción de una nación. Historia de la ingeniería civil en México en el siglo XIX"/></a>
                    <figcaption class="enlace"><a href="libro3.php" target="_parent">La construcción de una nación. Historia de la ingeniería civil en México en el siglo XIX</a></figcaption>

                </figure>
            </article>

            <article class="col-md-6 col-lg-3"> 
                <figure>

                    <a href="libro3.1.php" target="_parent"><img src="img/portada_3.1.jpg" alt="Apuntes de geoquímica"/></a>
                    <figcaption class="enlace"><a href="libro3.1.php" target="_parent">Apuntes de geoquímica</a></figcaption>

                </figure>
            </article>

        
        </section> <!-- /novedades -->

        <section id="mas_descargados" class="row mt-5">

            <h2>Más descargados</h2>

            <article class="col-md-6 col-lg-3">
                <figure>

                    <a href="libro4.php" target="_parent"><img src="img/portada_4.jpg" alt="Apuntes de Cálculo Integral"/></a>
                    <figcaption class="enlace"><a href="libro4.php" target="_parent">Apuntes de Cálculo Integral</a></figcaption>
                
                </figure>
            </article>

            <article class="col-md-6 col-lg-3">
                <figure>

                    <a href="libro5.php" target="_parent"><img src="img/portada_5.jpg" alt="Álgebra lineal y sus aplicaciones"/></a>
                    <figcaption class="enlace"><a href="libro5.php" target="_parent">Álgebra lineal y sus aplicaciones</a></figcaption>

                </figure>
            </article>

            <article class="col-md-6 col-lg-3">
                <figure>

                    <a href="libro6.php" target="_parent"><img src="img/portada_6.jpg" alt="Temas selectos de mecánica del medio continuo"/></a>
                    <figcaption class="enlace"><a href="libro6.php" target="_parent">Temas selectos de mecánica del medio continuo</a></figcaption>

                </figure>
            </article>

            <article class="col-md-6 col-lg-3"> </article>


        </section> <!-- /descargados -->

        <section id="recomendaciones" class="row mt-5">

            <h2>Recomendaciones</h2>

            <article class="col-md-6 col-lg-3">
                <figure>

                    <a href="libro7.php" target="_parent"><img src="img/portada_7.jpg" alt="Prácticas de laboratorio de la asignatura Dispositivos ópticos."/></a>
                    <figcaption class="enlace"><a href="libro7.php" target="_parent">Prácticas de laboratorio de la asignatura Dispositivos ópticos.</a></figcaption>

                </figure>
            </article>

            <article class="col-md-6 col-lg-3">
                <figure>

                    <a href="libro8.php" target="_parent"><img src="img/portada_8.jpg" alt="Apuntes de materiales peligrosos"/></a>
                    <figcaption class="enlace"><a href="libro8.php" target="_parent">Apuntes de materiales peligrosos</a></figcaption>

                </figure>
            </article>

            <article class="col-md-6 col-lg-3">
                <figure>

                    <a href="libro9.php" target="_parent"><img src="img/portada_9.jpg" alt="Fundamentos de seguridad informática"/></a>
                    <figcaption class="enlace"><a href="libro9.php" target="_parent">Fundamentos de seguridad informática</a></figcaption>
                
                </figure>
            </article>

            <article class="col-md-6 col-lg-3"> </article>

        </section> <!-- /recomendaciones -->

        <section id="multimedia" class="row mt-5">

        <h2>Videos</h2>
            <!--<a href="#">Ver todas las recomendaciones</a>-->

            <article class="col-md-12 col-lg-8">
                <video controls>
                    <source src="video/intro.mp4" type="video/mp4"/>

                        Tu navegador no soporta reproducción de video. 
                </video>
                <p>Nuevo portal de publicaciones</p>
                <br/>
            </article>

            <article class="col-md-12 col-lg-4">
                <audio controls>

                    <source src="audio/el_arte_en_la_fi.mp3" type="audio/mp3"/>

                    Tu navegador no soprta reproducción de audio

                </audio>
                <p>El Arte en la Facultad de Ingeniería<br/>
                Araceli Larrión Gallegos</p>
            </article>

        </section> <!-- /multimedia -->

        <!--<button onclick="topFunction()" id="myBtn" title="Go to top">Top</button>-->
        <a href="#demo" id="myBtn" title="Go to top">Subir</a>

    </main>

    <footer>

        <div class="footer-gris">

            <div>

                <img src="img/logofooter.png" alt="Facultad de Ingeniería"/>

                <div>

                    <input class="boton-contacto" type="button" value="Contacto" />
    
                </div>
            
            </div>

            <div>

                <p>
                    Universidad Nacional Autónoma de México<br/>
                    Facultad de Ingeniería, Av. Universidad 3000, Ciudad Universitaria,<br/> 
                    Coyoacán, Cd. Mx., CP 04510<br/>
                    Teléfono: 56 22 08 66<br/>
                    Fax: 56 16 28 90<br/>
                    <a href="mailto:fainge@unam.mx">fainge@unam.mx</a>

                    <div class="mt-3 text-center">
                        <a href="https://www.facebook.com/" target="blank"><img src="img/ico_face.jpg" data-aos="zoom-in" class="px-2 "></a>
                        <a href="https://twitter.com/?lang=es" target="blank"><img src="img/ico_twit.jpg" data-aos="zoom-in" class="px-2"></a>
                    </div>
                
                </p>
            
            </div>

            <div class="sitios-interes">

                <p>Sitios de interés</p>
                <ul>
                    <li><a href="solicitudes.php">Solicitudes de edición</a></li>
                    <li><a href="http://www.ptolomeo.unam.mx:8080/xmlui/" target="blank">Repositorio Digital FI</a></li>
                </ul>
            
            </div>

        </div>

        <div class="footer-rojo">

            <p>
                Esta página puede ser reproducida con fines no lucrativos, siempre y cuando no se mutile, se cite la fuente completa y su dirección electrónica. 
            </p>
        
        </div>
        

    </footer>


    <script>
        //Get the button
        var mybutton = document.getElementById("myBtn");
        
        // When the user scrolls down 20px from the top of the document, show the button
        window.onscroll = function() {scrollFunction()};
        
        function scrollFunction() {
          if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            mybutton.style.display = "block";
          } else {
            mybutton.style.display = "none";
          }
        }
        
        // When the user clicks on the button, scroll to the top of the document
        function topFunction() {
          document.body.scrollTop = 0;
          document.documentElement.scrollTop = 0;
        }
    </script>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

    <script src ="js/smooth.js"></script>
    <script src ="js/scroll_line.js"></script>


    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

    <script>
      AOS.init();
    </script>


</body>
</html>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Libros Facultad de Ingeniería UNAM</title>

    <link href="fonts/all.css" rel="stylesheet" />
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    
    <link href="css/comun.css" rel="stylesheet" />
    <link href="css/principal.css" rel="stylesheet" />

    

</head>

<body>

    <header id="demo">

        <div class="encabezado">

            <div class="escudos">

                <img src="img/escudo_unam_negro.png" alt="UNAM"/>
                <img src="img/escudo_fi_rojo.png" alt="FI"/>
            
            </div>

            <div class="buscador">

                <label for="buscar">Burcar</label>
                <input id="buscar" type="text" name="buscar" placeholder="Buscar por nombre, autor o título" maxlength="128" value="">
            
                <button type="submit" title="Buscar libro" alt="Burcar libro" class="fa-solid fa-magnifying-glass"><span>Buscar</span></button>
            </div>

            <div class="logo">
                <img src="img/logo_udae.png" alt="UDAE"/>
            </div>
            

        </div> <!-- /encabezado -->

        <nav class="menu-principal">

            <ul>
                <li><a href="index.php">Inicio</a></li>
                <li><a href="index.php#novedades" target="_parent">Novedades</a></li>
                <li><a href="index.php#mas_descargados" target="_parent">Más descargados</a></li>
                <li><a href="index.php#recomendaciones" target="_parent">Recomendaciones</a></li>
            </ul>

        </nav>   
        
        <div class="titulo-pagina">
            <h1>Catálogo</h1>
            
        </div>

    </header>

   

    <main class="container">        

        <section class="ver-mas row mt-5">

            <h2> </h2>

            <article class="d-flex flex-row justify-content-center alig-items-center col-md-6 col-lg-6">

                <a href="libro1.php" target="_parent" ><img src="img/portada_1.jpg" alt="Programación dinámica"/></a>

            </article>

            <article class="border-top border-4 my-auto col-md-6 col-lg-6">
                <div>
                    <li><b>Título: </b><em>Programación dinámica</em></li>
                    <li><b>Autor: </b>Idalia Flores de la Mota</li>
                    <li><b>Fecha de publicación:</b> febrero 2022</li>
                    <li><b>Área de conocimiento:</b> División de Ingeniería Mecánica e Industrial</li>
                </div>
            </article>

        
        </section> <!-- /ficha -->

        <section class="ver-mas row mt-5">

            <h2> </h2>

            <article class="d-flex flex-row justify-content-center alig-items-center col-md-6 col-lg-6">

                <a href="libro2.php" target="_parent"><img src="img/portada_2.jpg" alt="Evaluación de la calidad del agua mediante técnicas de percepción remota"/></a>

            </article>

            <article class="border-top border-4 my-auto col-md-6 col-lg-6">
                <div>
                    <li><b>Título: </b><em>Evaluación de la calidad del agua mediante técnicas de percepción remota</em></li>
                    <li><b>Autor: </b>Alba Beatriz Vázquez González, Rodrigo Takashi Sepúlveda Hirose y Vicente Fuentes Gea</li>
                    <li><b>Fecha de publicación:</b> enero 2022</li>
                    <li><b>Área de conocimiento:</b>  División de Ingenierías Civil y Geomática</li>
                </div>
            </article>

        
        </section> <!-- /ficha -->

        <section class="ver-mas row mt-5">

            <h2> </h2>

            <article class="d-flex flex-row justify-content-center alig-items-center col-md-6 col-lg-6">

                <a href="libro3.php" target="_parent"><img src="img/portada_3.jpg" alt="La construcción de una nación. Historia de la ingeniería civil en México en el siglo XIX"/></a>

            </article>

            <article class="border-top border-4 my-auto col-md-6 col-lg-6">
                <div>
                    <li><b>Título: </b><em>La construcción de una nación. Historia de la ingeniería civil en México en el siglo XIX</em></li>
                    <li><b>Autor: </b>Edgar Omar Rodríguez Camarena</li>
                    <li><b>Fecha de publicación:</b> noviembre 2021</li>
                    <li><b>Área de conocimiento:</b>  División de Ciencias Sociales y Humanidades</li>
                </div>
            </article>

        
        </section> <!-- /ficha -->

        <section class="ver-mas row mt-5">

            <h2> </h2>

            <article class="d-flex flex-row justify-content-center alig-items-center col-md-6 col-lg-6">

                <a href="libro3.1.php" target="_parent"><img src="img/portada_3.1.jpg" alt="Apuntes de geoquímica"/></a>

            </article>

            <article class="border-top border-4 my-auto col-md-6 col-lg-6">
                <div>
                    <li><b>Título: </b><em>Apuntes de geoquímica</em></li>
                    <li><b>Autor: </b>Laura Mori y Eduardo Becerra Torres</li>
                    <li><b>Fecha de publicación:</b> septiembre 2021</li>
                    <li><b>Área de conocimiento:</b> División de Ingeniería en Ciencias de la Tierra</li>
                </div>
            </article>

        
        </section> <!-- /ficha -->

        <section class="ver-mas row mt-5">

            <h2> </h2>

            <article class="d-flex flex-row justify-content-center alig-items-center col-md-6 col-lg-6">

                <a href="libro4.php" target="_parent"><img src="img/portada_4.jpg" alt="Apuntes de Cálculo Integral"/></a>

            </article>

            <article class="border-top border-4 my-auto col-md-6 col-lg-6">
                <div>
                    <li><b>Título: </b><em>Apuntes de Cálculo Integral</em></li>
                    <li><b>Autor: </b>Margarita Ramírez Galindo y María del Rocío Ávila Núñez</li>
                    <li><b>Fecha de publicación:</b> agosto 2021</li>
                    <li><b>Área de conocimiento:</b> División de Ciencias Básicas</li>
                </div>
            </article>

        
        </section> <!-- /ficha -->

        <section class="ver-mas row mt-5">

            <h2> </h2>

            <article class="d-flex flex-row justify-content-center alig-items-center col-md-6 col-lg-6">

                <a href="libro5.php" target="_parent"><img src="img/portada_5.jpg" alt="Álgebra lineal y sus aplicaciones"/></a>

            </article>

            <article class="border-top border-4 my-auto col-md-6 col-lg-6">
                <div>
                    <li><b>Título: </b><em>Álgebra lineal y sus aplicaciones</em></li>
                    <li><b>Autor: </b>Idalia Flores de la Mota</li>
                    <li><b>Fecha de publicación:</b> agosto 2021</li>
                    <li><b>Área de conocimiento:</b>División de Ingeniería Mecánica e Industrial</li>
                </div>
            </article>

        
        </section> <!-- /ficha -->

        <section class="ver-mas row mt-5">

            <h2> </h2>

            <article class="d-flex flex-row justify-content-center alig-items-center col-md-6 col-lg-6">

                <a href="libro6.php" target="_parent"><img src="img/portada_6.jpg" alt="Temas selectos de mecánica del medio continuo"/></a>

            </article>

            <article class="border-top border-4 my-auto col-md-6 col-lg-6">
                <div>
                    <li><b>Título: </b><em>Temas selectos de mecánica del medio continuo</em></li>
                    <li><b>Autor: </b>Ricardo Rubén Padilla Velázquez</li>
                    <li><b>Fecha de publicación:</b> agosto 2021</li>
                    <li><b>Área de conocimiento:</b>División de Ingenierís civil y Geomática</li>
                </div>
            </article>

        </section> <!-- /ficha -->

        <section class="ver-mas row mt-5">

            <h2> </h2>

            <article class="d-flex flex-row justify-content-center alig-items-center col-md-6 col-lg-6">

                <a href="libro7.php" target="_parent"><img src="img/portada_7.jpg" alt="Prácticas de laboratorio de la asignatura Dispositivos ópticos."/></a>

            </article>

            <article class="border-top border-4 my-auto col-md-6 col-lg-6">
                <div>
                    <li><b>Título: </b><em>Prácticas de laboratorio de la asignatura Dispositivos ópticos.</em></li>
                    <li><b>Autor: </b>Sergei Khotiaintsev </li>
                    <li><b>Fecha de publicación:</b> agosto 2021</li>
                    <li><b>Área de conocimiento:</b>División de Ingeniería Eléctrica</li>
                </div>
            </article>

        
        </section> <!-- /ficha -->

        <section class="ver-mas row mt-5">

            <h2> </h2>

            <article class="d-flex flex-row justify-content-center alig-items-center col-md-6 col-lg-6">

                <a href="libro8.php" target="_parent"><img src="img/portada_8.jpg" alt="Apuntes de Materiales peligrosos"/></a>

            </article>

            <article class="border-top border-4 my-auto col-md-6 col-lg-6">
                <div>
                    <li><b>Título: </b><em>Apuntes de Materiales peligrosos</em></li>
                    <li><b>Autor: </b>Luis Antonio García Villanueva, Omar César Sáncehz Domínguez, Gustavo Adolfo Montijo Muñoz y Dan Salazar Trujillo </li>
                    <li><b>Fecha de publicación:</b> agosto 2021</li>
                    <li><b>Área de conocimiento:</b>División de Ingenierías Civil y Geomática</li>
                </div>
            </article>

        
        </section> <!-- /ficha -->

        <section class="ver-mas row mt-5">

            <h2> </h2>

            <article class="d-flex flex-row justify-content-center alig-items-center col-md-6 col-lg-6">

                <a href="libro9.php" target="_parent"><img src="img/portada_9.jpg" alt="Fundamentos de seguridad informática"/></a>

            </article>

            <article class="border-top border-4 my-auto col-md-6 col-lg-6">
                <div>
                    <li><b>Título: </b><em>Fundamentos de seguridad informática</em></li>
                    <li><b>Autor: </b>Ma. Jaquelina López Barrientos y Cintia Quezada Reyes </li>
                    <li><b>Fecha de publicación:</b> noviembre 2019</li>
                    <li><b>Área de conocimiento:</b>División de Ingeniería Eléctrica</li>
                </div>
            </article>

        
        </section> <!-- /ficha -->


        <!--<button onclick="topFunction()" id="myBtn" title="Go to top">Top</button>-->
        <a href="#demo" id="myBtn" title="Go to top">Subir</a>

    </main>

    <footer>

        <div class="footer-gris">

            <div>

                <img src="img/logofooter.png" alt="Facultad de Ingeniería"/>

                <div>

                    <input class="boton-contacto" type="button" value="Contacto" />
    
                </div>
            
            </div>

            <div>

                <p>
                    Universidad Nacional Autónoma de México<br/>
                    Facultad de Ingeniería, Av. Universidad 3000, Ciudad Universitaria,<br/> 
                    Coyoacán, Cd. Mx., CP 04510<br/>
                    Teléfono: 56 22 08 66<br/>
                    Fax: 56 16 28 90<br/>
                    <a href="mailto:fainge@unam.mx">fainge@unam.mx</a>

                    <div class="mt-3 text-center">
                        <img src="img/ico_face.jpg" data-aos="zoom-in" class="px-2 ">
                        <img src="img/ico_twit.jpg" data-aos="zoom-in" class="px-2">
                        <img src="img/ico_g_plus.jpg" data-aos="zoom-in" class="px-2">
                    </div>
                
                </p>
            
            </div>

            <div class="sitios-interes">

                <p>Sitios de interés</p>
                <ul>
                    <li><a href="solicitudes.php">Solicitudes de edición</a></li>
                    <li><a href="http://www.ptolomeo.unam.mx:8080/xmlui/" target="blank">Repositorio Digital FI</a></li>
                </ul>
            
            </div>

        </div>

        <div class="footer-rojo">

            <p>
                Esta página puede ser reproducida con fines no lucrativos, siempre y cuando no se mutile, se cite la fuente completa y su dirección electrónica. 
            </p>
        
        </div>
        

    </footer>


    <script>
        //Get the button
        var mybutton = document.getElementById("myBtn");
        
        // When the user scrolls down 20px from the top of the document, show the button
        window.onscroll = function() {scrollFunction()};
        
        function scrollFunction() {
          if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            mybutton.style.display = "block";
          } else {
            mybutton.style.display = "none";
          }
        }
        
        // When the user clicks on the button, scroll to the top of the document
        function topFunction() {
          document.body.scrollTop = 0;
          document.documentElement.scrollTop = 0;
        }
    </script>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

    <script src ="js/smooth.js"></script>
    <script src ="js/scroll_line.js"></script>


    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

    <script>
      AOS.init();
    </script>


</body>
</html>


<?php

/* Valida correo */
function validarCorreo($correo) {
   
   $patronCorreo = preg_match("/^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/", $correo);

   return !empty($patronCorreo);
}

/* Valida textos*/
function validarTexto($texto) {
    
    $patronTexto = preg_match("/^[a-zA-Z\s]+$/", $texto);

   return !empty($patronTexto);
   
}

/* Valida teléfonos*/
function validarTel($tel) {
    
    $patronTel = preg_match("/^[0-9\s]+$/", $tel);

   return !empty($patronTel);
   
}

/*
* Verifica que el nombre de la imagen sea png/jpg
* @param $imagen Cadena con el nombre del archivo cargado
* @return true si el tipo de archivo es el correcto
*/
function validarImagen($imagen) {
   
  //$patronImagen = preg_match("/^[a-zA-Z0-9._-]+(.(jpg | png)) $/", $imagen['name']);


   $extension = substr($imagen['name'], strlen($imagen['name']) - 3 , strlen($imagen['name']));
   
   if(strcmp($extension, 'jpg')==0 || strcmp($extension, 'png')==0) {
      $ext = true;
   }
   else {
      $ext = false;
   }

   return !empty($ext);
   
}

/*
* Guarda la imagen cargada por el ususario
* en la ruta img/usuarios con el nombre 
* nombreUsuario.(extension)
* @return nombre del archivo cargado si se realizo correctamente la carga
*/
function cargarImagen($imagen, $nombreUsuario) {
  $archivoCargado = false;

  if (is_uploaded_file($imagen['tmp_name'])) {
     
   //Indicamos la carpeta donde será guardada la imagen y se le cambia el nombre como : nombre del usuario con extensión de la imagen.
   move_uploaded_file($imagen['tmp_name'],        
        "img/usuarios/$nombreUsuario." .  substr($imagen['name'], strlen($imagen['name']) - 3 , strlen($imagen['name']))
      );

      //Regresamos la direccion (ruta) donde está guardado el archivo
     $archivoCargado = "img/usuarios/$nombreUsuario." .  substr($imagen['name'], strlen($imagen['name']) - 3 , strlen($imagen['name']));
  }
  return $archivoCargado;
}
?>
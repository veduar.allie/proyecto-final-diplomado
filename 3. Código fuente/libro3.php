<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Libros Facultad de Ingeniería UNAM</title>

    <link href="fonts/all.css" rel="stylesheet" />
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    
    <link href="css/comun.css" rel="stylesheet" />
    <link href="css/principal.css" rel="stylesheet" />

    

</head>

<body>

    <header id="demo">

        <div class="encabezado">

            <div class="escudos">

                <img src="img/escudo_unam_negro.png" alt="UNAM"/>
                <img src="img/escudo_fi_rojo.png" alt="FI"/>
            
            </div>

            <div class="buscador">

                <label for="buscar">Burcar</label>
                <input id="buscar" type="text" name="buscar" placeholder="Buscar por nombre, autor o título" maxlength="128" value="">
            
                <button type="submit" title="Buscar libro" alt="Burcar libro" class="fa-solid fa-magnifying-glass"><span>Buscar</span></button>
            </div>

            <div class="logo">
                <img src="img/logo_udae.png" alt="UDAE"/>
            </div>
            

        </div> <!-- /encabezado -->

        <nav class="menu-principal">

            <ul>
                <li><a href="index.php">Inicio</a></li>
                <li><a href="catalogo.php">Catálogo</a></li> 
                <li><a href="index.php#novedades" target="_parent">Novedades</a></li>
                <li><a href="index.php#mas_descargados" target="_parent">Más descargados</a></li>
                <li><a href="index.php#recomendaciones" target="_parent">Recomendaciones</a></li>
            </ul>

        </nav>   
        
        <div class="titulo-pagina">
            <h1>Novedades</h1>
            
        </div>

    </header>

   

    <main class="container">        

        <section class="ver-mas row mt-5">

            <h2>La construcción de una nación. Historia de la ingeniería civil en México en el siglo XIX</h2>

            <article class="d-flex flex-row justify-content-center alig-items-center col-md-8 col-lg-8">

                <img src="img/libro_3.jpg" alt="La construcción de una nación. Historia de la ingeniería civil en México en el siglo XIX"/>

            </article>

            <article class="border-top border-4 my-auto col-md-4 col-lg-4">
                <div>
                    <li><b>Título: </b><em>La construcción de una nación. Historia de la ingeniería civil en México en el siglo XIX</em></li>
                    <li><b>Autor: </b>Edgar Omar Rodríguez Camarena</li>
                    <li><b>Fecha de publicación:</b> noviembre 2021</li>
                    <li><b>Área de conocimiento:</b>  División de Ciencias Sociales y Humanidades</li>
                </div>
            </article>

        
        </section> <!-- /ficha -->

        <section class="ver-mas row mt-5">

            
            <article class="">

                <p>México, Universidad Nacional Autónoma de México, Facultad de Ingeniería, 2021, 408 p. ISBN 978-607-30-5296-2. </p>

                <p>Obra electrónica disponible en:</p>
                    
                <p><a href="http://www.ptolomeo.unam.mx:8080/xmlui/handle/RepoFi/17609" target="_blank">http://www.ptolomeo.unam.mx:8080/xmlui/handle/RepoFi/17609</a></p>

                <p>La historia de la ingeniería civil en México se puede abordar desde dos vertientes principales. Por un lado, la formación profesional de los ingenieros civiles desde la introducción de estos estudios en el México independiente y su posterior desarrollo hasta el Porfiriato. Y, por otro lado, la labor constructiva a lo largo del mismo periodo, así como el desempeño y participación de los ingenieros civiles en el campo laboral. Para tener una comprensión más cabal en ambos campos, se analizan no sólo a los ingenieros civiles propiamente, sino también a otros profesionales involucrados en cuestiones constructivas, como arquitectos e ingenieros militares. Más allá de la significación propia de la vertiente académica o de la profesional, nos parece aún más importante la posibilidad de poder cotejarlas para, de esta manera, poder apreciar qué tanto la educación que se ofrecía a los ingenieros civiles era apropiada para un óptimo desempeño profesional.</p>

                <p>La construcción de una nación. Historia de la ingeniería civil en México en el siglo XIX es un libro de lectura obligada para los interesados en la historia de la ingeniería y de la arquitectura mexicana, así como de la historia de la educación en general pues, de una manera profunda, precisa y apropiada, el autor subsana un vacío en la historia de México con importante información inédita, magníficas ilustraciones, una descripción detallada y un análisis profundo de la evolución de estas disciplinas, y nos conduce por un doble pasaje histórico que nos remite al fluctuante e inestable contexto social, político y económico del país desde el periodo virreinal hasta el Porfiriato. Asimismo, mediante un meticuloso y lúcido análisis, caracteriza la evolución de la ingeniería civil desde la academia hasta el ejercicio laboral; para ello, nos presenta los elementos y factores que fueron entretejiendo desde las aulas hasta la esfera gubernamental, y de las relaciones establecidas con el círculo empresarial internacional en beneficio o perjuicio de su comunidad.</p>
                    
                <p><b>CONTENIDO.</b> Primera parte. La institucionalización de la ingeniería civil como carrera profesional: Antecedentes y primeros intentos de una formación en ingeniería; La introducción de la carrera de Ingeniería Civil en México. De la Academia de San Carlos a la Escuela de Ingenieros; La Ingeniería Civil y la Ingeniería de Caminos, Puertos y Canales durante el Porfiriato. Segunda parte. Desarrollo práctico de la construcción y desempeño profesional de los constructores: Primeros pasos en la construcción de una nación. La construcción durante la primera mitad del siglo XIX; La época de las reformas; Desarrollo constructivo porfiriano; Epílogo. Anexo 1; Anexo 2; Bibliografía; Referencias iconográficas. </p>
            </article>

           
        </section> <!-- /resumen -->


        <!--<button onclick="topFunction()" id="myBtn" title="Go to top">Top</button>-->
        <a href="#demo" id="myBtn" title="Go to top">Subir</a>

    </main>

    <footer>

        <div class="footer-gris">

            <div>

                <img src="img/logofooter.png" alt="Facultad de Ingeniería"/>

                <div>

                    <input class="boton-contacto" type="button" value="Contacto" />
    
                </div>
            
            </div>

            <div>

                <p>
                    Universidad Nacional Autónoma de México<br/>
                    Facultad de Ingeniería, Av. Universidad 3000, Ciudad Universitaria,<br/> 
                    Coyoacán, Cd. Mx., CP 04510<br/>
                    Teléfono: 56 22 08 66<br/>
                    Fax: 56 16 28 90<br/>
                    <a href="mailto:fainge@unam.mx">fainge@unam.mx</a>

                    <div class="mt-3 text-center">
                        <img src="img/ico_face.jpg" data-aos="zoom-in" class="px-2 ">
                        <img src="img/ico_twit.jpg" data-aos="zoom-in" class="px-2">
                        <img src="img/ico_g_plus.jpg" data-aos="zoom-in" class="px-2">
                    </div>
                
                </p>
            
            </div>

            <div class="sitios-interes">

                <p>Sitios de interés</p>
                <ul>
                    <li><a href="solicitudes.php">Solicitudes de edición</a></li>
                    <li><a href="http://www.ptolomeo.unam.mx:8080/xmlui/" target="blank">Repositorio Digital FI</a></li>
                </ul>
            
            </div>

        </div>

        <div class="footer-rojo">

            <p>
                Esta página puede ser reproducida con fines no lucrativos, siempre y cuando no se mutile, se cite la fuente completa y su dirección electrónica. 
            </p>
        
        </div>
        

    </footer>


    <script>
        //Get the button
        var mybutton = document.getElementById("myBtn");
        
        // When the user scrolls down 20px from the top of the document, show the button
        window.onscroll = function() {scrollFunction()};
        
        function scrollFunction() {
          if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            mybutton.style.display = "block";
          } else {
            mybutton.style.display = "none";
          }
        }
        
        // When the user clicks on the button, scroll to the top of the document
        function topFunction() {
          document.body.scrollTop = 0;
          document.documentElement.scrollTop = 0;
        }
    </script>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

    <script src ="js/smooth.js"></script>
    <script src ="js/scroll_line.js"></script>


    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

    <script>
      AOS.init();
    </script>


</body>
</html>


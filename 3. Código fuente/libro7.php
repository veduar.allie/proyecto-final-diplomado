<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Libros Facultad de Ingeniería UNAM</title>

    <link href="fonts/all.css" rel="stylesheet" />
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    
    <link href="css/comun.css" rel="stylesheet" />
    <link href="css/principal.css" rel="stylesheet" />

    

</head>

<body>

    <header id="demo">

        <div class="encabezado">

            <div class="escudos">

                <img src="img/escudo_unam_negro.png" alt="UNAM"/>
                <img src="img/escudo_fi_rojo.png" alt="FI"/>
            
            </div>

            <div class="buscador">

                <label for="buscar">Burcar</label>
                <input id="buscar" type="text" name="buscar" placeholder="Buscar por nombre, autor o título" maxlength="128" value="">
            
                <button type="submit" title="Buscar libro" alt="Burcar libro" class="fa-solid fa-magnifying-glass"><span>Buscar</span></button>
            </div>

            <div class="logo">
                <img src="img/logo_udae.png" alt="UDAE"/>
            </div>
            

        </div> <!-- /encabezado -->

        <nav class="menu-principal">

            <ul>
                <li><a href="index.php">Inicio</a></li>
                <li><a href="catalogo.php">Catálogo</a></li>
                <li><a href="index.php#novedades" target="_blank">Novedades</a></li>
                <li><a href="index.php#mas_descargados" target="_parent">Más descargados</a></li>
                <li><a href="index.php#recomendaciones" target="_parent">Recomendaciones</a></li>
            </ul>

        </nav>   
        
        <div class="titulo-pagina">
            <h1>Recomendaciones</h1>
            
        </div>

    </header>

   

    <main class="container">        

        <section class="ver-mas row mt-5">

            <h2>Prácticas de laboratorio de la asignatura Dispositivos ópticos.</h2>

            <article class="d-flex flex-row justify-content-center alig-items-center col-md-8 col-lg-8">

                <img src="img/libro_7.jpg" alt="Prácticas de laboratorio de la asignatura Dispositivos ópticos."/>

            </article>

            <article class="border-top border-4 my-auto col-md-4 col-lg-4">
                <div>
                    <li><b>Título: </b><em>Prácticas de laboratorio de la asignatura Dispositivos ópticos.</em></li>
                    <li><b>Autor: </b>Sergei Khotiaintsev </li>
                    <li><b>Fecha de publicación:</b> agosto 2021</li>
                    <li><b>Área de conocimiento:</b>División de Ingeniería Eléctrica</li>
                </div>
            </article>

        
        </section> <!-- /ficha -->

        <section class="ver-mas row mt-5">

            
            <article class="">

                <p>México, Universidad Nacional Autónoma de México, Facultad de Ingeniería, 2020, 54 p.  </p>

                <p>Obra electrónica disponible en:</p>
                    
                <p><a href="http://www.ptolomeo.unam.mx:8080/xmlui/handle/132.248.52.100/17275" target="_blank">http://www.ptolomeo.unam.mx:8080/xmlui/handle/132.248.52.100/17275</a></p>

                <p>Los protocolos para el desarrollo de las prácticas de Análisis espectral óptico y Filtrado óptico de señales presentan un original enfoque metodológico que promueve el trabajo autodidacta, el aprendizaje autónomo y la aplicación de los temas mediante actividades tanto presenciales en laboratorio como vía remota al ambiente de laboratorio, empleando para ello software de simulación, videos, animaciones y otros recursos de internet.</p>

                <p>La práctica de Análisis espectral óptico tiene como objetivo comprender el funcionamiento, construcción y características de espectrómetros ópticos, así como desarrollar habilidades para usar un espectrómetro óptico para la caracterización experimental de diversas fuentes de radiación óptica de los sistemas modernos de telecomunicaciones ópticas.</p>

                <p>La práctica de Filtrado óptico de señales permite analizar los principios de la colorimetría y el filtrado óptico, identificar los espectros de transmisión y absorción de dispositivos activos y pasivos de línea de fibra óptica, así como manejar eficientemente espectrofotómetros y técnicas de caracterización experimental de los espectros de distintos dispositivos ópticos.</p>

                <p>Para el desarrollo de las prácticas, se plantean cuatro fases: actividad previa de investigación teórica (prepráctica); reuniones virtuales para mostrar el equipo y material que se usará en la fase presencial, así como una demostración; actividad presencial para realizar los experimentos físicos reales, y la actividad posterior (pospráctica) para elaborar del informe, análisis de resultados y conclusiones.</p>
                    
                <p><b>CONTENIDO.</b> Prefacio, Forma de desarrollo de la práctica, Reglas de seguridad laboral; Práctica: Análisis espectral óptico, Referencias, Anexo CSS Series Operation Manual: Thorlabs Spectrometer CCS175; Práctica: Filtrado óptico de señales, Referencias, Anexo Espectrómetro de fibra óptica USB4000: Manual de instalación y operación. </p>
            </article>

           
        </section> <!-- /resumen -->


        <!--<button onclick="topFunction()" id="myBtn" title="Go to top">Top</button>-->
        <a href="#demo" id="myBtn" title="Go to top">Subir</a>

    </main>

    <footer>

        <div class="footer-gris">

            <div>

                <img src="img/logofooter.png" alt="Facultad de Ingeniería"/>

                <div>

                    <input class="boton-contacto" type="button" value="Contacto" />
    
                </div>
            
            </div>

            <div>

                <p>
                    Universidad Nacional Autónoma de México<br/>
                    Facultad de Ingeniería, Av. Universidad 3000, Ciudad Universitaria,<br/> 
                    Coyoacán, Cd. Mx., CP 04510<br/>
                    Teléfono: 56 22 08 66<br/>
                    Fax: 56 16 28 90<br/>
                    <a href="mailto:fainge@unam.mx">fainge@unam.mx</a>

                    <div class="mt-3 text-center">
                        <img src="img/ico_face.jpg" data-aos="zoom-in" class="px-2 ">
                        <img src="img/ico_twit.jpg" data-aos="zoom-in" class="px-2">
                        <img src="img/ico_g_plus.jpg" data-aos="zoom-in" class="px-2">
                    </div>
                
                </p>
            
            </div>

            <div class="sitios-interes">

                <p>Sitios de interés</p>
                <ul>
                    <li><a href="solicitudes.php">Solicitudes de edición</a></li>
                    <li><a href="http://www.ptolomeo.unam.mx:8080/xmlui/" target="blank">Repositorio Digital FI</a></li>
                </ul>
            
            </div>

        </div>

        <div class="footer-rojo">

            <p>
                Esta página puede ser reproducida con fines no lucrativos, siempre y cuando no se mutile, se cite la fuente completa y su dirección electrónica. 
            </p>
        
        </div>
        

    </footer>


    <script>
        //Get the button
        var mybutton = document.getElementById("myBtn");
        
        // When the user scrolls down 20px from the top of the document, show the button
        window.onscroll = function() {scrollFunction()};
        
        function scrollFunction() {
          if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            mybutton.style.display = "block";
          } else {
            mybutton.style.display = "none";
          }
        }
        
        // When the user clicks on the button, scroll to the top of the document
        function topFunction() {
          document.body.scrollTop = 0;
          document.documentElement.scrollTop = 0;
        }
    </script>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

    <script src ="js/smooth.js"></script>
    <script src ="js/scroll_line.js"></script>


    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

    <script>
      AOS.init();
    </script>


</body>
</html>


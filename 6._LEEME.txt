a. Nombre completo de la alumna
Véduar Allié Sarmiento Torres

b. Nombre del proyecto
Sitio web para la difusión y descarga de las publicaciones elaboradas por el personal académico de la Facultad de Ingeniería de la UNAM, dirigido a la comunidad universitaria.

c. Correo electrónico de la alumna
allie@unam.mx

d. Dirección electrónica de su proyecto, alojado en servidores de DGTIC, no se aceptarán proyectos alojados en servidores externos.
http://132.248.75.198/~cuenta17/index.php


e. Nombres de usuario y contraseña en caso de que el sitio Web tenga algún sistema de autenticación o cualquier otro dato necesario para la revisión.
Sistema de autenticación: No aplica

Cuenta con la sección "Solicitudes de edición" con enlace al pie de la página; dicha sección se conforma por un formulario que procesa la recepción de datos y los guarda en la base de datos.
